terraform {
  backend "s3" {
    bucket  = "aztek.terraform.tfstate"
    key     = "vpc/terraform.tfstate"
    region  = "us-west-2"
    encrypt = "true"
  }
}
